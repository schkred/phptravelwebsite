package com.qagroup.phptravelpages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HotelDetailsPage {

	private WebDriver driver;

	@FindBy(css = "div.panel-heading.go-text-right.panel-inverse.ttu")
	private WebElement availableRooms;
	
	@FindBy(css = "strong.ellipsis.ttu")
	private WebElement hotelNameTitle;

	public HotelDetailsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	public boolean isAvailableRoomsDisplayed() {
		return availableRooms.isDisplayed();
	}
	
	public String getHotelNameTitle() {
		return hotelNameTitle.getText();
	}
}
