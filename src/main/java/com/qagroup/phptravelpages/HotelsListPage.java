package com.qagroup.phptravelpages;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.qameta.allure.Attachment;
import io.qameta.allure.Step;

public class HotelsListPage {

	private WebDriver driver;

	@FindBy(css = "td.wow.fadeIn.p-10-0.animated")
	private List<WebElement> hotelsList;

	@FindBy(css = ".list_title>a")
	private List<WebElement> hotelNamesList;

	//getting the list of Strings to use as a test data
	// ------------------------------------------------
	public List<String> getHotelNamesTextList() {
		List<String> hotelNames = new ArrayList<>();
		for (WebElement e:hotelNamesList) {
			String name = e.getText();
			hotelNames.add(name);
		}
		return hotelNames;
	}

	@Attachment("Random Hotel Name")
	public String selectRandomHotelName() {
		Random random = new Random();
		String randomName = hotelNamesList.get(random.nextInt(hotelNamesList.size())).getText();
		return randomName;
	}
	//-------------------------------------------------

	public HotelsListPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public HotelDetailsPage openHotelByNumber(int hotelNumber) {
		WebElement firstHotelInList = hotelsList.get(hotelNumber - 1);
		WebElement previewPicture = firstHotelInList.findElement(By.cssSelector(".img_list"));
		previewPicture.click();
		return new HotelDetailsPage(driver);
	}

	@Step("Open a hotel by the given name <{hotelName}>")
	public HotelDetailsPage openHotelByName(String hotelName) {
		WebElement firstHotelInList = hotelNamesList.stream().filter(e -> e.getText().contains(hotelName)).findFirst()
				.get();

		firstHotelInList.click();
		return new HotelDetailsPage(driver);

	}
}