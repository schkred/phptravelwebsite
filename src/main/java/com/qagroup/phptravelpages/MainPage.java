package com.qagroup.phptravelpages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qagroup.tools.Browser;

import io.qameta.allure.Step;

public class MainPage {

	private WebDriver driver;
	
	@FindBy(css = ".navbar-static-top #li_myaccount > a")
	private WebElement myAccountButton;

	@FindBy(css = ".navbar-static-top #li_myaccount > a + .dropdown-menu")
	private WebElement myAccountDropdown;
	
	@FindBy(css = "ul.nav.navbar-nav.navbar-left")
	private WebElement topNavigationBar;

	public MainPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}

	public LoginPage openLoginPage() {
		driver = Browser.open();
		myAccountButton.click();
		WebElement loginButton = myAccountDropdown.findElement(By.xpath(".//a[contains(text(),'Login')]"));
		loginButton.click();
		return new LoginPage(driver);
	}

	@Step("Open hotels list page")
	public HotelsListPage openHotelsListPage() {
		WebElement hotelsButton = topNavigationBar.findElement(By.xpath(".//li//a[text()[contains(., 'Hotels')]]"));
		hotelsButton.click();
		return new HotelsListPage(driver);
	}

}
