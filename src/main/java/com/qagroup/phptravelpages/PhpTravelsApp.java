package com.qagroup.phptravelpages;

import org.openqa.selenium.WebDriver;

import com.qagroup.tools.Browser;

import io.qameta.allure.Step;

public class PhpTravelsApp {

	private WebDriver driver;

	@Step("Open main page")
	public MainPage openMainPage() {
		driver = Browser.open();
		driver.get("http://www.phptravels.net/");
		return new MainPage(driver);
	}

	@Step("Close application")
	public void close() {
		if (driver != null) {
			driver.quit();
			driver = null;
		}
	}
}
