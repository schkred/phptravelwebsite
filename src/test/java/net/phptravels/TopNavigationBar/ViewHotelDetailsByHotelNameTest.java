package net.phptravels.TopNavigationBar;

import org.apache.bcel.generic.Select;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.qagroup.phptravelpages.*;

public class ViewHotelDetailsByHotelNameTest {

	private HotelDetailsPage hotelDetailsPage;

	private PhpTravelsApp phpTravelsApp;

	private MainPage mainPage;

	private HotelsListPage hotelListPage;

	private String hotelName;

	@Test
	public void viewHotelDetailsByHotelName() {
		phpTravelsApp = new PhpTravelsApp();
		mainPage = phpTravelsApp.openMainPage();
		hotelListPage = mainPage.openHotelsListPage();
		waitFor(2);
		hotelName = hotelListPage.selectRandomHotelName();
		hotelDetailsPage = hotelListPage.openHotelByName(hotelName);
		waitFor(2);
		// hotelDetailsPage =
		// phpTravelsApp.openMainPage().openHotelsListPage().openHotelByName("Swissotel
		// Le Plaza Basel");

		Assert.assertEquals(hotelDetailsPage.getHotelNameTitle(), hotelName);
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown() {
		phpTravelsApp.close();
	}

	private void waitFor(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
